#!/bin/bash

PACKAGES_POSIX="sdl2 sdl2-image sdl2-gfx sdl2-ttf sdl2-mixer sdl2-net"
for word in $PACKAGES_POSIX
do 
	PACKAGES_WIN=${PACKAGES_WIN}${word}":x64-windows "
done

POSIX=TRUE

case "$(uname)" in 
	*linux*|*darwin*|*msys*|*cygwin*|*bsd*) POSIX=TRUE ;;
	*win*|*nt*|*WIN*|*NT*) POSIX=FALSE && WIN=TRUE ;;
esac

if [ $POSIX = TRUE ]
then
	./vcpkg/bootstrap-vcpkg.sh -disableMetrics
	./vcpkg/vcpkg install ${PACKAGES_POSIX}
else
	./vcpkg/bootstrap-vcpkg.bat -disableMetrics
	./vcpkg/vcpkg.exe install ${PACKAGES_WIN}
fi
