#ifndef TIMER_DEBUG
#define TIMER_DEBUG

#include <chrono>
#include <cstdio>
#include <ctime>
#include <string>

class StopWatch
{
  std::chrono::_V2::system_clock::time_point start;
  std::chrono::_V2::system_clock::time_point stop;
  std::chrono::high_resolution_clock         clock;
  std::string m_msg;

public:
  StopWatch(std::string const &msg = "");
  ~StopWatch();
};

#endif