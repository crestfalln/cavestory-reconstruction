#include "timer.h"

namespace utils
{
Timer & Timer::getInstance()
{
  static Timer instance;
  return instance;
}
auto Timer::now() -> time_point_type
{
  using namespace std::chrono;
  auto &timer = getInstance();
  return time_point_cast<duration_type>(timer.clock.now());
}
} // namespace utils