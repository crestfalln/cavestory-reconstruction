#include "game.h"
#include "shapes.h"
#include "timer.h"
#include "utility.h"
#include <SDL2/SDL2_gfxPrimitives.h>
#include <iostream>
#ifdef DEBUG_BENCH
#include "timer_debug.h"
#endif

/*
int main(int argc, char **args)
{
  utils::Polygon poly1{{12,23},{21,255}, {103, 44} , {55 , 30}};
  utils::Circle circle1({0,0}, 1234);
  std::cout << utils::testIntersection(circle1 , poly1);
}
*/

int main()
{
  utils::Polygon quad1({{0, 0},
                        {200 * 5, 50 * 5},
                        {100 * 5, 100 * 5},
                        {300 * 5, 300 * 5},
                        {400 * 5, 80 * 5},
                        {320 * 5, 40 * 5}});
  utils::Circle  circle1({1000, 1000}, 500 / 2);
  /*
  std::cerr << quad1.testPoint({320*2  - 400, 40*2}) << std::endl;

      for(int i = 0 ; i <= quad1.getBounds().second.second; ++i)
      {
        for(int j = 0 ; j <= quad1.getBounds().second.first; ++j)
        {
          auto toTest = quad1.getBounds().first + utils::GenericPoint<int>{j,i};
          if(quad1.testPoint(toTest))
          {
            std::cout << "*";
          }
          else {
            std::cout << " ";
          }
        }
        std::cout << "\n";
      }
 */
  short vx[6];
  short vy[6];
  int   i = 0;
  for (auto const &iter : quad1.getVerticies())
  {
    vx[i] = iter.i();
    vy[i] = iter.j();
    ++i;
  }
  Graphics graphics("Title", {2560, 1440});
#ifdef DEBUG_BENCH
  {
    StopWatch stp("GFX fill: ");
#endif
    //filledCircleRGBA(graphics.m_renderer, circle1.getCenter().i(),
    //                 circle1.getCenter().j(), circle1.getRadius(), 255, 0, 0,
    //                 100);
    //filledPolygonRGBA(graphics.m_renderer, vx, vy, 6, 255, 0, 0, 100);
#ifdef DEBUG_BENCH
  }
#endif
#ifdef DEBUG_BENCH
  {
    StopWatch stp("My Fill: ");
#endif
    auto     tex    = graphics.makeTextureFromShape(quad1);
    auto     bounds = quad1.getBounds();
    SDL_Rect rect{bounds.first.i(), bounds.first.j(), bounds.second.first,
                  bounds.second.second};
    SDL_RenderCopy(graphics.m_renderer, tex, nullptr, &rect);
#ifdef DEBUG_BENCH
  }
#endif
  SDL_RenderPresent(graphics.m_renderer);
  utils::wait(utils::Timer::s{10});
}