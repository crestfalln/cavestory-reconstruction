#include "game.h"
#include "SDL_render.h"
#include "timer.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>

Game::Game()
    : m_graphics("v1.0.0", { 960, 960 })
{
    SDL_Init(SDL_INIT_AUDIO | SDL_INIT_EVENTS | SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER | SDL_INIT_HAPTIC);
    IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG | IMG_INIT_WEBP | IMG_INIT_TIF);
    SDL_RendererInfo info;
    SDL_GetRendererInfo(m_graphics.m_renderer, &info);
    std::cout << info.name << std::endl;
}

void Game::mainLoop(int frameCap)
{
    SDL_Event                         event;
    utils::Timer::time_point_type     first, second;
    const utils::Timer::duration_type minDelta { (1000000 / frameCap) };
    utils::Timer::duration_type       delta { minDelta };
    utils::Timer::duration_type       frameTime { minDelta };
    utils::Timer::duration_type       accumulator { 0 };
    Image img("file/1.bmp", m_graphics);
    bool                              quit = 0;

    img.render();
    while (!quit)
    {
        first = utils::Timer::now();
        utils::wait(utils::Timer::us{1});
        while (SDL_PollEvent(&event) > 0)
        {
            if (event.type == SDL_QUIT)
            {
                quit = true;
                break;
            }
            else if (event.type == SDL_KEYDOWN)
            {
                std::cout << "Here" << std::endl;
                SDL_SetRenderDrawColor(m_graphics.m_renderer, 255, 0, 0, 255);
            }
        }
        if (accumulator >= minDelta)
        {
            accumulator = static_cast<decltype(accumulator)>(0);
            SDL_RenderPresent(m_graphics.m_renderer);
        }
        second    = utils::Timer::now();
        delta     = second - first;
        frameTime = delta;
        accumulator += delta;
    }
}

Game::~Game()
{
    IMG_Quit();
    SDL_Quit();
}