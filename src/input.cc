#include "input.h"
#include <SDL_events.h>

void inputs::newFrame()
{
	m_pressed_keys.clear();	
	m_released_keys.clear();
}

void inputs::keyDown(const SDL_Event& event)
{
    m_pressed_keys[event.key.keysym.scancode] = 1;
    m_held_keys[event.key.keysym.scancode] = 1;
}

void inputs::keyUp(const SDL_Event& event)
{
    m_held_keys[event.key.keysym.scancode] = 0;
    m_released_keys[event.key.keysym.scancode] = 1;
}

bool inputs::handleKeyEvent(const SDL_Event & event)
{
    switch (event.type) 
    {
        case SDL_KEYDOWN:
            inputs::keyDown(event);
            return true;
        case SDL_KEYUP:
            inputs::keyUp(event);
            return true;
        default:
            return false;
    }
}