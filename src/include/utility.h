#ifndef UTILITY_H
#define UTILITY_H

#include <array>
#include <cmath>
#include <initializer_list>
#include <memory>
#include <vector>

namespace utils
{

template <typename _T> int signum(_T const &num)
{
  if (num > _T(0))
    return 1;
  if (num < _T(0))
    return -1;
  return 0;
}
template <typename _T> bool sign(_T const &num)
{
  if (num < _T(0))
    return 1;
  return 0;
}

template <typename _T> _T square(_T const &num) { return num * num; }

template <typename _T> struct Vector : protected std::pair<_T, _T>
{
  using std::pair<_T, _T>::pair;
  Vector normalize()
  {
    auto   mag = magnitude();
    Vector res;
    res.first /= mag;
    res.second /= mag;
    return res;
  }
  _T absolute() const
  {
    return (Vector::first * Vector::first + Vector::second * Vector::second);
  }
  _T     magnitude() const { return std::sqrt(absolute()); }
  Vector operator+(Vector const &second) const
  {
    return {this->first + second.first, this->second + second.second};
  }
  Vector operator*(_T const &num) const
  {
    return {this->first * num, this->second * num};
  }
  Vector operator/(_T const &num) const
  {
    return {this->first / num, this->second / num};
  }
  bool operator==(Vector const &second) const
  {
    return this->first == second.first && this->second == second.second;
  }
  _T &   i() { return this->first; };
  _T &   j() { return this->second; };
  _T     i() const { return this->first; };
  _T     j() const { return this->second; };
  Vector operator-(Vector const &second) const { return *this + (second * -1); }
  template <typename _N>
  friend _N dotProduct(Vector<_N> const &one, Vector<_N> const &two);
  template <typename _N>
  friend Vector<_N> crossProduct(Vector<_N> const &one, Vector<_N> const &two);
};

template <typename _T>
_T dotProduct(Vector<_T> const &one, Vector<_T> const &two)
{
  return (one.first * two.first) + (one.second * two.second);
};

template <typename _T>
_T crossProduct(Vector<_T> const &one, Vector<_T> const &two)
{
  return (one.first * two.second) - (two.first * one.second);
}

template <typename _T>
_T distanceAbs(Vector<_T> const &first, Vector<_T> const &second)
{
  return (first - second).absolute();
}

template <typename _T>
_T distance(Vector<_T> const &first, Vector<_T> const &second)
{
  return (first - second).magnitude();
}

template <typename _T> using GenericPoint = Vector<_T>;
using HighPrecisionVector                 = Vector<double>;
using LowPrecisionVector                  = Vector<float>;
using HPV                                 = HighPrecisionVector;
using LPV                                 = LowPrecisionVector;

} // namespace utils
#endif
