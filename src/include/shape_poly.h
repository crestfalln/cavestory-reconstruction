#ifndef SHAPE_POLY_H
#define SHAPE_POLY_H

#include "shape_interface.h"
#include "utility.h"

namespace utils
{

template <typename _T> struct GenericPoly : public GenericShape<_T>
{
  using Point = GenericPoint<_T>;

  std::vector<Point>                  m_verticies;
  _T                                  m_area;
  Point                               m_center;
  _T                                  m_r;
  Point const *                       m_furthest;
  Point                               m_com;
  std::pair<Point, std::pair<_T, _T>> m_bounds;

  GenericPoly(std::initializer_list<Point> verticies)
      : m_verticies(verticies), m_center({0, 0}), m_r(-1)
  {
    Point max{m_verticies[0]}, min{m_verticies[0]};
    m_verticies.shrink_to_fit();
    for (auto const &iter : m_verticies)
    {
      if (max.i() < iter.i())
        max.i() = iter.i();
      if (max.j() < iter.j())
        max.j() = iter.j();
      if (min.i() > iter.i())
        min.i() = iter.i();
      if (min.j() > iter.j())
        min.j() = iter.j();
      m_center = m_center + iter;
    }
    m_center               = m_center / m_verticies.size();
    m_bounds.first         = min;
    m_bounds.second.first  = max.i() - min.i();
    m_bounds.second.second = max.j() - min.j();
  }

  virtual Point getCenter() const override { return m_center; }
  virtual _T    getArea() const override { return m_area; }
  virtual std::pair<Point, std::pair<_T, _T>> getBounds() const override
  {
    return m_bounds;
  }
  virtual int countVertices() const override { return m_verticies.size(); }
  virtual std::vector<Point> const &getVerticies() const override
  {
    return m_verticies;
  }
  virtual _T getRadius() override
  {
    if (m_r != -1)
      return m_r;
    _T abs = 0;
    for (auto &iter : getVerticies())
    {
      auto max_candidate = distanceAbs(getCenter(), iter);
      if (max_candidate > abs)
      {
        abs        = max_candidate;
        m_furthest = &iter;
      }
    }
    m_r = std::sqrt(abs);
    return m_r;
  }

  virtual _T getRadius() const override { return m_r; }

  virtual Point getFurthest() const override { return *m_furthest; }

  virtual Point getFurthest() override
  {
    getRadius();
    return *m_furthest;
  }

  /** @brief Uses Crossing Number Test to check if a point is inside a
   *         poly
   *  @param Point to be checked in utils::Point form
   *  @return True if point in poly, false otherwise
   */
  virtual bool testPoint(Point const &point) const override
  {
    int counter = 0;

    // Checks the position of origin wrt line
    //  1 Left
    //  0 On
    // -1 Right
    auto pos_line = [](Point const &shift1, Point const &shift2)
    {
      return signum((shift1.i() * (shift2.j() - shift1.j())) -
                    (shift1.j() * (shift2.i() - shift1.i()))) *
             signum(shift2.j() - shift1.j());
    };

    // Checks if + horizontal ray originating param point
    // intersects polygon edge given by verticies {i, j}
    // Adds 1 to the counter for all such intersections
    auto unit_test = [&](int const &i, int const &j)
    {
      Point shift_first  = getVerticies()[i] - point;
      Point shift_second = getVerticies()[j] - point;
      if (shift_first.j() >= 0 && shift_second.j() < 0 ||
          shift_first.j() <= 0 && shift_second.j() > 0)
      {
        if (shift_first.i() > 0 & shift_second.i() > 0 ||
            pos_line(shift_first, shift_second) > 0)
          ++counter;
      }
    };

    for (int i = 0; i < getVerticies().size() - 1; ++i)
      unit_test(i, i + 1);
    unit_test(getVerticies().size() - 1, 0);

    // Even counter: outside
    // Odd counter: inside
    if (counter % 2)
      return true;
    return false;
  }
  virtual Point move(Vector<_T> const &shift) override
  {
    for (auto &iter : m_verticies)
      iter = iter + shift;
    m_center       = m_center + shift;
    m_bounds.first = m_bounds.first + shift;
    return m_center;
  }
};

template <typename _T> struct GenericQuad : public GenericPoly<_T>
{
  GenericQuad(GenericPoint<_T> const &a, GenericPoint<_T> const &b,
              GenericPoint<_T> const &c, GenericPoint<_T> const &d)
      : GenericPoly<_T>({a, b, c, d})
  {
  }
};

using Polygon = GenericPoly<int>;
using Quad    = GenericQuad<int>;
}; // namespace utils

#endif