#ifndef SHAPE_INTERFACE_H
#define SHAPE_INTERFACE_H

#include "utility.h"

namespace utils
{
template <typename _T> struct GenericShape
{
  using Point                                       = GenericPoint<_T>;
  using Points                                      = std::vector<Point>;
  virtual bool  testPoint(Point const &point) const = 0;
  virtual int   countVertices() const               = 0;
  virtual Point move(Vector<_T> const &shift)       = 0;
  virtual _T    getArea() const                     = 0;
  virtual Point getCenter() const                   = 0;
  virtual _T    getRadius() const                   = 0;
  virtual Point getFurthest() const                 = 0;
  virtual _T    getRadius()                         = 0;
  virtual std::pair<Point, std::pair<_T, _T>> getBounds() const    = 0;
  virtual Point                               getFurthest()        = 0;
  virtual Points const &                      getVerticies() const = 0;
};

template <typename _T1, typename _T2>
bool testIntersection(GenericShape<_T1> &shape1, GenericShape<_T2> &shape2)
{
  if (shape1.countVertices() == -1 && shape2.countVertices() == -1)
    return distanceAbs(shape1.getCenter(), shape2.getCenter()) <=
           square(shape1.getRadius() + shape2.getRadius());
  auto &low_poly  = shape1;
  auto &high_poly = shape2;
  if (low_poly.countVertices() > high_poly.countVertices())
  {
    low_poly  = shape2;
    high_poly = shape1;
  }
  for (auto const &iter : high_poly.getVerticies())
  {
    if (low_poly.testPoint(iter))
      return true;
  }
  return false;
}

template <typename _T1, typename _T2, typename _Tt>
GenericShape<_Tt> *intersection(_T1, _T2);

using Shape = GenericShape<int>;

}; // namespace utils

#endif