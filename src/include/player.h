#ifndef PLAYER_H
#define PLAYER_H

#include "graphics.h"
#include "utility.h"
#include <SDL2/SDL_rect.h>
#include <unordered_map>

class stationary_entity
{
protected:
    utils::Vector<double> m_pos;
    SDL_Rect              m_rect;

public:
    auto getPos() -> utils::Vector<double>;
};

class animated_entity : public stationary_entity
{
protected:
    std::unordered_map<std::string, Animation> m_animations;
};

class moving_entity : public animated_entity
{
protected:
    utils::Vector<double> m_speed;
};

class npc : public moving_entity
{

};

class player : public moving_entity
{
protected:
    utils::Vector<double> m_speed;
};

#endif