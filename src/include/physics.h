#include "utility.h"
#include "timer.h"
#include <cmath>

// Common Interface for all objects
class BasicGameObject
{
  int placeholder;
};

// Interface for physics objects
class BasicPhysicsObject : BasicGameObject
{
  virtual void setVelocity(utils::HPV const &vel)     = 0;
  virtual void setAcceleration(utils::HPV const &acc) = 0;
	virtual void act(utils::Timer::duration_type const &delta);
};

// Interface for static objects
class BasicStaticObject : BasicGameObject
{
};