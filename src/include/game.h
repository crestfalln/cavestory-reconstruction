#ifndef GAME_H
#define GAME_H

#include <SDL2/SDL.h>
#include <graphics.h>
#include <utility>


class Game
{
private:
	Graphics m_graphics;

public:
    void mainLoop(int frameCap = 60);
	Game();
	~Game();
};

#endif