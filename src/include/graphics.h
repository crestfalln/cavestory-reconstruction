#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "timer.h"
#include "utility.h"
#include <utility>
#include <vector>
#include "shape_interface.h"

struct SDL_Window;
struct SDL_Renderer;
struct SDL_Texture;



/*
 * Class That Handles Graphics
 */
class Graphics
{
public:
    SDL_Window*   m_window;
    SDL_Renderer* m_renderer;
    Graphics(std::string const& title, std::pair<int, int> const& res, std::pair<int, int> const& pos = { -1, -1 });
    Graphics(Graphics const&) = delete;
    Graphics(Graphics&&);
    SDL_Texture* makeTextureFromShape(utils::Shape const &shape);
    void operator=(Graphics const&) = delete;
    void operator                   =(Graphics&&);
    Graphics()                      = delete;
    ~Graphics();
};

class Image
{
    std::string  m_path;
    SDL_Texture* m_tex = nullptr;
    Graphics&    m_graphics;

public:
    Image(std::string const& path, Graphics&);
    Image(SDL_Texture* tex, Graphics&);
    Image(Image&&) noexcept;
    Image()              = delete;
    Image(Image const&)  = delete;
    void         operator=(Image const& img) = delete;
    void         operator                    =(Image&& img) noexcept;
    SDL_Texture* getTexture();
    void         load();
    Graphics&    getGraphics();
    void         render();
    ~Image();
};

class Animation
{
public:
    using ImageArray = std::vector<Image>;

private:
    ImageArray                  m_assets;
    utils::Timer::duration_type m_animation_time;
    utils::Timer::duration_type m_minDelta;
    utils::Timer::duration_type m_accumulator;
    Graphics&                   m_graphics;
    size_t                      m_frame_no = 0;

public:
    void renderCurrent();
    void renderNext();
    void render(utils::Timer::duration_type accumulator);
    void resetFrameCounter();
    Animation(ImageArray&& assets, utils::Timer::duration_type animation_time);
    auto getMinFrameTime() -> utils::Timer::duration_type;
};

#endif